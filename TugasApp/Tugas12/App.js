import { StatusBar } from 'expo-status-bar';
import React, { Component } from 'react'
import {StyleSheet, View, Text, Image, TouchableOpacity, FlatList} from 'react-native'
import { FontAwesome } from '@expo/vector-icons';
import { MaterialIcons } from '@expo/vector-icons';
import VideoItem from './components/videoitem';
import data from './data.json';




export default class App extends Component {
    render() {
        return (
            <View style={styles.container}>
               <View style= {styles.navBar}>
                   <Image source={require('./images/logo.png')} style={{width: 98, height: 22}}/>
                   <View style={styles.rightNav}>
                    <TouchableOpacity>
                   <FontAwesome style={styles.navItems} name="search" size= {25}/>
                   </TouchableOpacity>
                   <TouchableOpacity>
                   <MaterialIcons style={styles.navItems} name="account-circle" size={25}/>
                   </TouchableOpacity>
            </View>
        </View>
                    <View style={styles.Body}>
                    <FlatList
                        data={data.items}
                        renderItem={(video)=><VideoItem video={video.item} />}
                        keyExtractor={(item)=>item.id}
                        ItemSeparatorComponent={()=><View style={{height:0.5,backgroundColor:'#E5E5E5'}}/>}

                        />
                    </View>
                    <View style= {styles.tabBar}>
                        <TouchableOpacity style={styles.tabItems}>
                            <FontAwesome name="home" size={25}/>
                            <Text style={styles.tabTittle}>Home</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.tabItems}>
                            <MaterialIcons name="whatshot" size={25}/>
                            <Text style={styles.tabTittle}>Trending</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.tabItems}>
                            <MaterialIcons name="subscriptions" size={25}/>
                            <Text style={styles.tabTittle}>Subscriptions</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.tabItems}>
                            <MaterialIcons name="snippet-folder" size={25}/>
                            <Text style={styles.tabTittle}>Library</Text>
                        </TouchableOpacity>
                    
               </View>
            </View>
        )
    }
};

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    navBar: {
        height: 80,
        backgroundColor: 'white',
        elevation: 3,
        paddingHorizontal: 15,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    rightNav: {
        flexDirection: 'row'
    }, 
    navItems: {
        marginLeft: 30
    },
    Body: {
        flex: 1
    },
    tabBar: {
        backgroundColor: 'white',
        height: 60,
        borderTopWidth: 0.5,
        borderColor: '#E5E5E5',
        flexDirection: 'row',
        justifyContent: 'space-around'
    },
    tabItems: {
        alignItems: 'center',
        justifyContent: 'center'
    },
    tabTittle: {
        fontSize: 11,
        color: '#3C3C3C',
        paddingTop: 4

    }
})
