import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

import Tugas12 from './Tugas12/App'
import Tugas13LoginScreen from './Tugas13/LoginScreen'
import Tugas13SignUpScreen from './Tugas13/SignUpScreen'
import Tugas13AboutScreen from './Tugas13/About'
import Tugas14 from './Tugas14/App'
import Tugas15 from './Tugas15/index'
import Tugas15_2 from './Tugas15/Tugas15-2/Index'

export default function App() {
  return (
    //<Tugas12/>
    //<Tugas13LoginScreen/>
    //<Tugas13SignUpScreen/>
    //<Tugas13AboutScreen/>
    //<Tugas14/>
    //<Tugas15/>
    <Tugas15_2/>
    
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
